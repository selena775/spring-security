

/*
create table user_auth (id bigint NOT NULL AUTO_INCREMENT, name varchar(255) NOT NULL, password varchar(255), primary key (id));
create table user_roles (user_id bigint not null, role varchar(255))
alter table user_auth add constraint UK_tjfiwvq97cdjhvgg57wno0n0e  unique (name);
alter table user_roles add constraint FK_g1uebn6mqk9qiaw45vnacmyo2 foreign key (user_id) references user_auth(id);
*/

insert into user_auth (name,password) values  ( 'student','stu', );
insert into user_auth (name,password) values  ( 'alice','ali' );
insert into user_auth (name,password) values  ( 'bob','bob' );
insert into user_auth (name,password) values  ( 'mark','mar' );
insert into user_auth (name,password) values  ( 'anonymous','' );

insert into user_roles (user_id,role) values ((select id from user_auth where name = 'student'), 'user');
insert into user_roles (user_id,role) values ((select id from user_auth where name = 'student'), 'student');

insert into user_roles (user_id,role) values ((select id from user_auth where name = 'alice'), 'user');

insert into user_roles (user_id,role) values ((select id from user_auth where name = 'bob'), 'user');
insert into user_roles (user_id,role) values ((select id from user_auth where name = 'bob'), 'admin');
insert into user_roles (user_id,role) values ((select id from user_auth where name = 'mark'), 'user');
